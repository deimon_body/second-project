import createMovieBlock from "../createMovieBlock/createMovieBlock.js";
import fetchModel from "../fetchModel/fetchModel.js";
const upCammingWrapper = document.querySelector(".upComming__block");
const url = "https://api.themoviedb.org/3/movie/upcoming";
const errText = "So sorry,but some mistakes with upComming films :)";
let pageNum = 1;
async function renderUpCommingFilms(api, paginate) {
    try {
        let data;
        if (paginate) {
            pageNum += 1;
            data = await fetchModel(api, upCammingWrapper, url, pageNum);
        }
        else {
            data = await fetchModel(api, upCammingWrapper, url);
        }
        getUpponComingMovies(data.results);
    }
    catch (error) {
        if (upCammingWrapper) {
            upCammingWrapper.innerHTML = `<p style="margin-top:30px;">${errText}</p>`;
        }
    }
}
function getUpponComingMovies(data) {
    for (let i = 0; i < 10; i++) {
        const elem = createMovieBlock(data[i].poster_path, "popular-movies__item", data[i].id, data[i].original_title);
        upCammingWrapper?.appendChild(elem);
    }
}
export default renderUpCommingFilms;
