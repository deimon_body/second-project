import createMovieBlock from '../createMovieBlock/createMovieBlock.js';
const movieEl = document.querySelector(".currentMovie");
const url = "https://api.themoviedb.org/3/search/movie";
async function getMovieFunc(title, api) {
    try {
        let res = await fetch(`${url}?api_key=${api}&language=en-US&query=${title}&page=1&include_adult=false`);
        let request = await res.json();
        renderMovie(request);
    }
    catch (error) {
        if (movieEl) {
            movieEl.innerHTML = '<p style="margin-top:30px;">So sorry,but your film didn`t found.Try to write right title of film :)</p>';
            return;
        }
    }
}
function createTextEl(el, text) {
    const elem = document.createElement(el);
    elem.textContent = text;
    return elem;
}
function createMovieCart(movieInfo) {
    const movieBlock = createMovieBlock(movieInfo.poster_path, "movie-cart__img", movieInfo.id, movieInfo.original_title);
    const wrapperDiv = document.createElement("div");
    wrapperDiv.classList.add("movie-cart");
    const textBlock = document.createElement("div");
    textBlock.classList.add("movie-cart__text-block");
    const movieTitle = createTextEl("h2", movieInfo.title);
    const movieYear = createTextEl("p", movieInfo.release_date);
    const movieDescription = createTextEl("p", movieInfo.overview);
    textBlock.appendChild(movieTitle);
    textBlock.appendChild(movieYear);
    textBlock.appendChild(movieDescription);
    wrapperDiv.appendChild(movieBlock);
    wrapperDiv.appendChild(textBlock);
    return wrapperDiv;
}
function renderMovie(data) {
    movieEl.innerHTML = "";
    movieEl.appendChild(createMovieCart(data.results[0]));
}
export default getMovieFunc;
