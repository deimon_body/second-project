const url = 'https://api.themoviedb.org/3/movie/';
async function getMovieInfo(api, id) {
    try {
        let res = await fetch(`${url}${{ id }}?api_key=${api}&language=en-US`);
        let request = await res.json();
        return request;
    }
    catch (error) {
        throw new Error("Wrong id of movie :(");
    }
}
export default getMovieInfo;
