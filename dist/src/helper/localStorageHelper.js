function lStorageSet(id, info) {
    localStorage.setItem(id, info);
}
function lStorageDelete(id) {
    localStorage.removeItem(id);
}
function lStorageHasId(id) {
    if (localStorage.getItem(id)) {
        return true;
    }
    return false;
}
function lStorageGetInfo(id) {
    return localStorage.getItem(id);
}
export default { lStorageSet, lStorageHasId, lStorageGetInfo, lStorageDelete };
