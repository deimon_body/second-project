import getMovieFunc from "./helper/getMovie/getMovie.js";
import renderPopularMovies from "./helper/getPopularFilm/getPopularFilm.js";
import renderBestMovies from "./helper/getTheBest/getTheBest.js";
import renderUpCommingFilms from "./helper/getUpComming/getUpComming.js";
import renderRandomMovie from "./helper/getRandomMovie/getRandomMovie.js";
import paginate from "./helper/pagination/pagination.js";

const getMovieBtn = document.querySelector(".search-current-movie__button");
const getMovieTitle = document.querySelector(".search-current-movie__input") as HTMLInputElement;
const api_key:string = "abddd9821131bd64f255ccc171554771";
const popularBtn = document.querySelector(".getPopularMoreBtn");
const bestBtn = document.querySelector(".best-movies-btn");
const upCommingBtn = document.querySelector(".upComming-btn");

const getMovie = ()=>{
    const title:string|undefined|null = getMovieTitle?.value;
    if(title){
        getMovieFunc(title,"abddd9821131bd64f255ccc171554771")
    }
}


popularBtn?.addEventListener("click",()=>{
    paginate(renderPopularMovies,api_key)
})
bestBtn?.addEventListener("click",()=>{
    paginate(renderBestMovies,api_key)
})
upCommingBtn?.addEventListener('click',()=>{
    paginate(renderUpCommingFilms,api_key)  
})

renderPopularMovies(api_key);
renderBestMovies(api_key);
renderUpCommingFilms(api_key);
renderRandomMovie(api_key)

getMovieBtn?.addEventListener("click",getMovie);