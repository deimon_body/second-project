function lStorageSet(id:string,info:string){
    localStorage.setItem(id,info)
}

function lStorageDelete(id:string){
    localStorage.removeItem(id);
}

function lStorageHasId(id:string):Boolean{
    if(localStorage.getItem(id)){
        return true
    }
    return false
    
}

function lStorageGetInfo(id:string):string|null{
    return localStorage.getItem(id)
}


export default {lStorageSet,lStorageHasId,lStorageGetInfo,lStorageDelete};