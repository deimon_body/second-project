import fetchModel from "../fetchModel/fetchModel.js";
import createMovieBlock from "../createMovieBlock/createMovieBlock.js";

const randomWrapper:HTMLElement|null = document.querySelector('.banner__block');
const url = 'https://api.themoviedb.org/3/movie/popular'

async function renderRandomMovie(api:string){
    try{
        let data = await fetchModel(api,randomWrapper,url)
        getRandomMovie(data.results);
    }
    catch(err){
        if(randomWrapper){
            randomWrapper.innerHTML = "Sorry,but some error with random film :)";
        }
    }
}

function getRandomMovie(data:any){
    let currentData = data[Math.floor(Math.random()*data.length)];
    randomWrapper?.appendChild(createMovieBlock(currentData.poster_path,'banner-item',currentData.id,currentData.original_title));
}

export default renderRandomMovie;