import createMovieBlock from "../createMovieBlock/createMovieBlock.js";
import fetchModel from "../fetchModel/fetchModel.js";

const movieWrapper:HTMLElement|null =  document.querySelector(".popular-movies__block");
const url:string = "https://api.themoviedb.org/3/movie/popular"
const errText = "So sorry,but some mistakes with Popular films :)"
let pageNum:number = 1;

async function renderPopularMovies(api:string,paginate?:boolean){
    try{
        let data;
        if(paginate){
            pageNum+=1;
            data = await fetchModel(api,movieWrapper,url,pageNum)
        }else{
            data = await fetchModel(api,movieWrapper,url)
        }
        
        getPopularMovies(data.results)
    }
    catch(error){ 
        if(movieWrapper){
            movieWrapper.innerHTML = `<p style="margin-top:30px;">${errText}</p>`;
        }   
    }

}



function getPopularMovies(data:any):void{
    for(let i=0;i<data.length;i++){
        const elem = createMovieBlock(data[i].poster_path,"popular-movies__item",data[i].id,data[i].original_title);
        movieWrapper?.appendChild(elem);
    }

}

export default renderPopularMovies;