async function fetchModel(api:string,wrapper:HTMLElement|null,url:string,pageNum:number=1){
    
    try{
        let res = await fetch(`${url}?api_key=${api}&language=en-US&page=${pageNum}`);
        let request = await res.json();
        return request
           
    }
    catch(error){
        throw new Error();
    }
}


export default fetchModel; 