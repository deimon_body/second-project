import createMovieBlock from "../createMovieBlock/createMovieBlock.js";
import fetchModel from "../fetchModel/fetchModel.js";

const bestWrapper:HTMLElement|null =  document.querySelector(".best-movies__block");
const url:string = "https://api.themoviedb.org/3/movie/top_rated"
const errText:string = "So sorry,but some mistakes with Popular films :)"
let pageNum:number = 1;

async function renderBestMovies(api:string,paginate?:boolean){
    try{
        let data;
        if(paginate){
            pageNum+=1;
            data = await fetchModel(api,bestWrapper,url,pageNum)
        }else{
            data = await fetchModel(api,bestWrapper,url)
        }
        getBetsMovies(data.results)  
    }
    catch(error){
        if(bestWrapper){
            bestWrapper.innerHTML = `<p style="margin-top:30px;">${errText}</p>`;
            return
        }
    }
}


function createFilmCart(data:any):HTMLElement{
    const movieBlock = createMovieBlock(data.poster_path,'best-movies__item',data.id,data.original_title)
    const rating = document.createElement("p");
    rating.classList.add("raiting")
    rating.textContent = `Rating:${data.vote_average}`;

    movieBlock.appendChild(rating);
    return movieBlock;
}

function getBetsMovies(data:any):void{
    for(let i=0;i<10;i++){
        bestWrapper?.appendChild(createFilmCart(data[i]));
    }

}

export default renderBestMovies;