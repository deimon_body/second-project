import createMovieBlock from '../createMovieBlock/createMovieBlock.js';

const movieEl:HTMLElement = document.querySelector(".currentMovie") as HTMLElement;
const url:string = "https://api.themoviedb.org/3/search/movie";


async function getMovieFunc(title:string,api:string){
    try{
        let res = await fetch(`${url}?api_key=${api}&language=en-US&query=${title}&page=1&include_adult=false`);
        let request = await res.json()
        renderMovie(request)   
    }
    catch(error){
        if(movieEl){
            movieEl.innerHTML = '<p style="margin-top:30px;">So sorry,but your film didn`t found.Try to write right title of film :)</p>';
            return
        }
    }
}

function createTextEl(el:string,text:string):HTMLElement{
    const elem:HTMLElement = document.createElement(el);
    elem.textContent = text;
    return elem
}

function createMovieCart(movieInfo:any):HTMLElement{
    
    const movieBlock = createMovieBlock(movieInfo.poster_path,"movie-cart__img",movieInfo.id,movieInfo.original_title)
    const wrapperDiv:HTMLElement = document.createElement("div");
    wrapperDiv.classList.add("movie-cart")
    const textBlock:HTMLElement = document.createElement("div");
    textBlock.classList.add("movie-cart__text-block")
    
    const movieTitle:HTMLElement = createTextEl("h2",movieInfo.title)
    const movieYear:HTMLElement = createTextEl("p",movieInfo.release_date);
    const movieDescription:HTMLElement = createTextEl("p",movieInfo.overview);

    textBlock.appendChild(movieTitle);
    textBlock.appendChild(movieYear);
    textBlock.appendChild(movieDescription);

    wrapperDiv.appendChild(movieBlock)
    wrapperDiv.appendChild(textBlock)

    return wrapperDiv
}

function renderMovie(data:any):void{
    movieEl.innerHTML = "";
    movieEl.appendChild(createMovieCart(data.results[0]))

}

export default getMovieFunc