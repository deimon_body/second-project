import createMovieBlock from "../createMovieBlock/createMovieBlock.js";
import fetchModel from "../fetchModel/fetchModel.js";

const upCammingWrapper:HTMLElement|null =  document.querySelector(".upComming__block");
const url:string = "https://api.themoviedb.org/3/movie/upcoming"
const errText:string = "So sorry,but some mistakes with upComming films :)"
let pageNum:number = 1;

async function renderUpCommingFilms(api:string,paginate?:boolean){
    try{
        let data;
        if(paginate){
            pageNum+=1;
            data = await fetchModel(api,upCammingWrapper,url,pageNum)
        }else{
            data = await fetchModel(api,upCammingWrapper,url)
        }
        getUpponComingMovies(data.results)
    }
    catch(error){ 
        if(upCammingWrapper){
            upCammingWrapper.innerHTML = `<p style="margin-top:30px;">${errText}</p>`;
        }   
    }

}


function getUpponComingMovies(data:any):void{
    for(let i=0;i<10;i++){
        const elem = createMovieBlock(data[i].poster_path,"popular-movies__item",data[i].id,data[i].original_title);
        upCammingWrapper?.appendChild(elem);
    }

}

export default renderUpCommingFilms;